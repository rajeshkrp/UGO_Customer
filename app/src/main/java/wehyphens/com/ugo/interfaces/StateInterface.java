package wehyphens.com.ugo.interfaces;

import java.util.List;

import wehyphens.com.ugo.Models.CountryCodeDetails;

public interface StateInterface {
    public  void setValue(String name, String id,String flag,boolean selected);

    void setValue(List<CountryCodeDetails> currentSelected, int type);
}
