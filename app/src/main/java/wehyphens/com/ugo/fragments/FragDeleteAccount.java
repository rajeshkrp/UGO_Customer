package wehyphens.com.ugo.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import wehyphens.com.ugo.R;
import wehyphens.com.ugo.activities.EditProfileActivity;
import wehyphens.com.ugo.activities.PrivacyPolicyActivity;

public class FragDeleteAccount extends Fragment {
Context mContext;
TextView header_two_title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_delete_account, container, false);
        mContext=getActivity();
        header_two_title=getActivity().findViewById(R.id.header_two_title);
        header_two_title.setText("Delete Account");
        return view;
    }

}
