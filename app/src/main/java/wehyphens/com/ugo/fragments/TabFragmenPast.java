package wehyphens.com.ugo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import wehyphens.com.ugo.Adapters.FragPastTripsAdapter;
import wehyphens.com.ugo.Adapters.YourTripsTabAdapter;
import wehyphens.com.ugo.Models.PastTripsModel;
import wehyphens.com.ugo.R;

/**
 * Created by Admin on 3/5/2018.
 */

public class TabFragmenPast extends Fragment {
    RecyclerView recyclerView;
    FragPastTripsAdapter adapter;
    PastTripsModel listModel;
    ArrayList<PastTripsModel> arrayList;
    Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_fragment_past_trips, container, false);

        recyclerView=view.findViewById(R.id.recycler_view);
        arrayList=new ArrayList<>();
        mContext=getActivity();

        for(int i=0;i<10;i++){
            listModel=new PastTripsModel();
            listModel.setDateTime("22/06/18, 5:19 pm");
           listModel.setRupees("0.00");
           listModel.setVehiclename("Swift dezire");
           listModel.setStatus("Cancelled");
            arrayList.add(listModel);
        }

        adapter=new FragPastTripsAdapter(arrayList,getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(mContext,DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
        return view;
    }

}
