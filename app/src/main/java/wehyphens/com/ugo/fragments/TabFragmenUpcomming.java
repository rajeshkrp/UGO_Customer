package wehyphens.com.ugo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import wehyphens.com.ugo.Adapters.YourTripsTabAdapter;
import wehyphens.com.ugo.R;

/**
 * Created by Admin on 3/5/2018.
 */

public class TabFragmenUpcomming extends Fragment {
    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_fragment_upcomming, container, false);

        recyclerView=view.findViewById(R.id.recycler_view);

        return view;
    }

}
