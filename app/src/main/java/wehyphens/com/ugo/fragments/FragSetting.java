package wehyphens.com.ugo.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import wehyphens.com.ugo.Adapters.FragPastTripsAdapter;
import wehyphens.com.ugo.Models.PastTripsModel;
import wehyphens.com.ugo.R;
import wehyphens.com.ugo.activities.EditProfileActivity;
import wehyphens.com.ugo.activities.PrivacyPolicyActivity;
import wehyphens.com.ugo.activities.SignUpactivity;
import wehyphens.com.ugo.utils.AppConstants;
import wehyphens.com.ugo.utils.CommonUtils;

public class FragSetting extends Fragment {
Context mContext;
    FirebaseAuth mAuth;
    GoogleApiClient mGoogleApiClient;
TextView username,usereamil,tvphone;
RelativeLayout rl_top_pannel,rl_privacy_policy,relsignout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_setting, container, false);
        mContext=getActivity();

        username=view.findViewById(R.id.tv_user_name);
        usereamil=view.findViewById(R.id.tv_user_email);
        tvphone=view.findViewById(R.id.tv_phn);
        relsignout=view.findViewById(R.id.rel_signout);


        tvphone.setText(CommonUtils.getPreferencesString(mContext,AppConstants.USER_PHONE2));
//        usereamil.setText(CommonUtils.getPreferencesString(mContext,AppConstants.USER_EMAIL));

        rl_top_pannel=view.findViewById(R.id.rl_top_pannel);
        rl_privacy_policy=view.findViewById(R.id.rl_privacy_policy);

        rl_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,PrivacyPolicyActivity.class));
            }
        });

        rl_top_pannel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,EditProfileActivity.class));
            }
        });


relsignout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        signOut();
    }
});
        return view;
    }

    private void signOut()
    {
        mAuth.signOut();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {

                mGoogleApiClient.disconnect();
                Log.d("status",status+"");
                //  Toast.makeText(getApplicationContext(),status+"",Toast.LENGTH_SHORT).show();
                Intent i=new Intent(getActivity().getApplicationContext(),SignUpactivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                SharedPreferences preferences =getActivity().getSharedPreferences("loginPrefs",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                getActivity().finish();
            }
        });
    }

}
