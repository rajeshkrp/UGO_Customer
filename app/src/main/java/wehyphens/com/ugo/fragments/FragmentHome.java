package wehyphens.com.ugo.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import wehyphens.com.ugo.R;
import wehyphens.com.ugo.activities.ConfirmBookingActivity;
import wehyphens.com.ugo.activities.SignUpactivity;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;
import static android.support.v4.content.ContextCompat.checkSelfPermission;

public class FragmentHome extends Fragment implements OnMapReadyCallback, LocationListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    Context mContext;
    TextView title_name;
    Activity mactivity;
    private GoogleMap mMap;
    LinearLayout ll_select_rider;
    RelativeLayout rl_pick_location;
    ImageView iv_plus;
    EditText et_loc_distination, met_pick_location;
    TextView tv_user, tv_Cname;
    ImageView iv_who_ride;
    boolean flag = false;
    TextView tv_Cnbr;
    private final int PICK_CONTACT = 1;
    private String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS};
    ArrayList<String> phoneContactList;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    GoogleApiClient mGoogleApiClient;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    LocationRequest mLocationRequest;
    RelativeLayout relativeLayout;



    private static final String TAG = FragmentHome.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_CONTACTS = 1;
    private Uri uriContact;
    private String contactID;
    Map<String, String> namePhoneMap = new HashMap<String, String>();
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    public static final int REQUEST_READ_CONTACTS = 79;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = getActivity();
        mactivity = getActivity();
        //  title_name=getActivity().findViewById(R.id.title_name);
        // title_name.setText("Locations");

        et_loc_distination = view.findViewById(R.id.et_loc_distination);
        rl_pick_location = view.findViewById(R.id.rl_pick_location);
        ll_select_rider = view.findViewById(R.id.ll_select_rider);
        tv_user = getActivity().findViewById(R.id.tv_user);
        met_pick_location = view.findViewById(R.id.et_pick_location);
        relativeLayout =view.findViewById(R.id.rela_par);

//        NavigationView navigationView = (NavigationView) getView().findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);


        DrawerLayout drawer = (DrawerLayout) view.findViewById(R.id.drawer_layout);



        iv_who_ride = getActivity().findViewById(R.id.iv_drop_down_user);
        tv_Cname = view.findViewById(R.id.tv_Cname1);

        tv_Cnbr = view.findViewById(R.id.tv_Cnbr);
        iv_plus = view.findViewById(R.id.iv_plus);


        if (ActivityCompat.checkSelfPermission(mactivity, android.Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
           // getContacts();
        } else {
            requestLocationPermission();  //calling runtime permission method
        }


        iv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Clicked",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
               startActivityForResult(intent, 1);

               // startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), REQUEST_READ_CONTACTS);


            }
        });

        iv_who_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag == false) {
                    rl_pick_location.setVisibility(View.GONE);
                    ll_select_rider.setVisibility(View.VISIBLE);
                    tv_user.setText("Switch User");
                    flag = true;
                } else {

                    rl_pick_location.setVisibility(View.VISIBLE);
                    ll_select_rider.setVisibility(View.GONE);
                    tv_user.setText("For Users");
                    flag = false;
                }

            }
        });


        et_loc_distination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRequest();

                startActivity(new Intent(getActivity(), ConfirmBookingActivity.class));

            }
        });

        et_loc_distination.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {


                return false;
            }
        });

//        et_loc_distination.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//`
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//              //  startActivity(new Intent(getActivity(), ConfirmBookingActivity.class));
//                // TODO Auto-generated method stub
//            }
//        });

        initializeMap();

        return view;

    }


    private void sendRequest() {
        String origin = met_pick_location.getText().toString();
        String destination = et_loc_distination.getText().toString();
        if (origin.isEmpty()) {
            Toast.makeText(mactivity, "Please enter origin address!", Toast.LENGTH_SHORT).show();
        }
        if (destination.isEmpty()) {
            Toast.makeText(mactivity, "Please enter destination address!", Toast.LENGTH_SHORT).show();
        }

    }

    private void initializeMap() {
        if (mMap == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFrag.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(51.5, -0.1), new LatLng(40.7, -74.0))
                .width(5)
                .color(Color.RED));

        setUpMap();
    }

    private void setUpMap() {

//        LatLng WeHyphens_Private_Limited = new LatLng(28.585704, 77.314605);
//        mMap.addMarker(new MarkerOptions().position(WeHyphens_Private_Limited).title("WeHyphens Private Limited"));
//
//        LatLng latLngg = new LatLng(28.584598, 77.314320);
//        mMap.addPolyline(new PolylineOptions().add(
//                WeHyphens_Private_Limited,
//                new LatLng(28.585462, 77.314743),
//                new LatLng(28.585182, 77.314245),
//                new LatLng(28.584696, 77.314597),
//                latLngg
//                )
//                        .width(10)
//                        .color(Color.RED)
//        );
//
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(WeHyphens_Private_Limited, 18));
//
//        if (checkSelfPermission(mactivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(mactivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        mMap.setMyLocationEnabled(true);
//        mMap.getUiSettings().setMyLocationButtonEnabled(true);
////        //  mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);


        mMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
        mMap.setOnMyLocationClickListener(onMyLocationClickListener);
        enableMyLocationIfPermitted();

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMinZoomPreference(11);
    }

    private void enableMyLocationIfPermitted() {
        if (ContextCompat.checkSelfPermission(mactivity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mactivity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }

    }

    private void showDefaultLocation() {
        Toast.makeText(mactivity, "Location permission not granted, " +
                        "showing default location",
                Toast.LENGTH_SHORT).show();
        LatLng redmond = new LatLng(47.6739881, -122.121512);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(redmond));
    }


    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    protected void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mactivity,
                android.Manifest.permission.READ_CONTACTS)) {
            // show UI part if you want here to show some rationale !!!

        } else {

            ActivityCompat.requestPermissions(mactivity, new String[]{android.Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);
        }
    }



    //runtime permission method  for contact
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocationIfPermitted();
                } else {
                    showDefaultLocation();
                }
                return;
            }
        }
    }


    private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
            new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    mMap.setMinZoomPreference(15);
                    return false;

                }
            };


    private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {

                    mMap.setMinZoomPreference(12);

                    CircleOptions circleOptions = new CircleOptions();
                    circleOptions.center(new LatLng(location.getLatitude(),
                            location.getLongitude()));

                    circleOptions.radius(30);
                    circleOptions.fillColor(Color.CYAN);
                    circleOptions.strokeWidth(2);
                    mMap.addCircle(circleOptions);
                }
            };

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mactivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_PICK_CONTACTS) {
            if (resultCode == RESULT_OK) {
                Uri contactData = data.getData();
                Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(contactData, null, null, null, null);
                cursor.moveToFirst();

                String name=cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String number = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));

                tv_Cname.setText(name);
                tv_Cnbr.setText(number);
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(mactivity,
                (String) android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

       // Toast.makeText(mactivity, "Please connect to the internt!", Toast.LENGTH_SHORT).show();

    }


}
