package wehyphens.com.ugo.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import wehyphens.com.ugo.fragments.TabFragmenPast;
import wehyphens.com.ugo.fragments.TabFragmenUpcomming;

/**
 * Created by user on 2/15/2018.
 */

public class YourTripsTabAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public YourTripsTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
               // TabFragUserSignIn tab1=new TabFragUserSignIn();
                TabFragmenPast tab1=new TabFragmenPast();

                return tab1;
            case 1:
               // TabFragmentSignUp tab2 = new TabFragmentSignUp();
                TabFragmenUpcomming tab2=new TabFragmenUpcomming();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
