package wehyphens.com.ugo.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import wehyphens.com.ugo.Models.PastTripsModel;
import wehyphens.com.ugo.R;

/**
 * Created by user on 2/16/2018.
 */

public class FragPastTripsAdapter extends RecyclerView.Adapter<FragPastTripsAdapter.ViewHolder> {

    PastTripsModel dealerListModel;
    ArrayList<PastTripsModel> arrayList = new ArrayList<>();

    Context context;

    public FragPastTripsAdapter(ArrayList<PastTripsModel> arrayList, Context context) {


        this.arrayList = arrayList;

        this.context = context;

    }

    @Override
    public FragPastTripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;


            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_past_trips_row_item, parent, false);


        FragPastTripsAdapter.ViewHolder recyclerViewHolder = new FragPastTripsAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(FragPastTripsAdapter.ViewHolder holder, int position) {

        holder.tv_date_time.setText(arrayList.get(position).getDateTime());
        holder.tv_rupees.setText(arrayList.get(position).getRupees());
        holder.tv_vehicle_name.setText(arrayList.get(position).getVehiclename());
        holder.tv_status.setText(arrayList.get(position).getStatus());
       // holder.pro_name2.setText(arrayList.get(position).getProductName());


    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

       TextView tv_date_time,tv_rupees,tv_vehicle_name,tv_status;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_date_time=itemView.findViewById(R.id.tv_date_time);
            tv_rupees=itemView.findViewById(R.id.tv_rupees);
            tv_vehicle_name=itemView.findViewById(R.id.tv_vehicle_name);
            tv_status=itemView.findViewById(R.id.tv_status);




        }
    }
    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter((color), PorterDuff.Mode.SRC_IN));
            }
        }
    }
}
