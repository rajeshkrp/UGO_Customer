package wehyphens.com.ugo.utils;

import java.util.List;

import wehyphens.com.ugo.Models.Route;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
