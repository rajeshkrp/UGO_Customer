package wehyphens.com.ugo.utils;

import android.net.Uri;

import java.net.URI;

public class AppConstants {
    public static final String USER_NAME="user_name";
    public static final String USER_EMAIL="user_email";
    public static final String USER_PHONE="user_phone";
    public static final String USER_PHONE2="user_phone2";
    public static final String USER_PHONE3="user_phone3";
    public static final Uri USER_PHOTO= Uri.parse("user_photo");

}
