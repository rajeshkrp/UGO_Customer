package wehyphens.com.ugo.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import wehyphens.com.ugo.R;

public class CommonUtils {

    public final static String FISRT_TIME_LOGIN = "first_time_login";
    private static Dialog dialog;

    public static void snackBar(String s, View v) {
        try {
            Snackbar snackbar = Snackbar.make(v, s, Snackbar.LENGTH_LONG)
                    .setAction("action", null);
            snackbar.getView().setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    //Shave Google info

    public static void savePreferencesString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPreferencesString(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(key, "");
    }


}
