package wehyphens.com.ugo.retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static Retrofit retrofit;
    public static Retrofit getRetrofit(){

        retrofit=new Retrofit.Builder()
                .baseUrl("http://wehyphens.com/hid_messenger/webservices/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();

        return retrofit;
    }

}
