package wehyphens.com.ugo.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import wehyphens.com.ugo.DataInterface.FileUploadInterface;

public class RetrofitHandler {

    private static RetrofitHandler uniqInstance;
    private FileUploadInterface apiInterface;
    //private final String BASE_URL = "http://wehyphens.com/hid_messenger/webservices/";
    private final String BASE_URL = "http://wehyphens.com/hid_messenger/webservices/";

    public static synchronized RetrofitHandler getInstance() {
        if (uniqInstance == null) {
            uniqInstance = new RetrofitHandler();
        }
        return uniqInstance;
    }


    private void ApiClient() {
        try {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

            // set up log type
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(50, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();

            apiInterface = retrofit.create(FileUploadInterface.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public FileUploadInterface getApi() {
        if (apiInterface == null) {
            uniqInstance.ApiClient();
        }
        return apiInterface;
    }


}
