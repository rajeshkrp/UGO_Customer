package wehyphens.com.ugo.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIInterface {

    @GET("list.php")
    Call<ResponseBody> getCatagoryList();

    @POST("list.php")
    @FormUrlEncoded
    Call<ResponseBody> getSubCatagoryList(@Field("cat_id") String catagory);


}
