package wehyphens.com.ugo.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import wehyphens.com.ugo.R;

public class AccountTripUpdateUpdate extends AppCompatActivity {
Context mContext;
TextView header_two_title;
ImageView iv_back;
RelativeLayout rl_account_trip_updates,rl_dis_news;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_account_trip_update);
        iv_back=findViewById(R.id.iv_back);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mContext=AccountTripUpdateUpdate.this;
        header_two_title=findViewById(R.id.header_two_title);

        header_two_title.setText("Account And Trip Updates");

    }
}
