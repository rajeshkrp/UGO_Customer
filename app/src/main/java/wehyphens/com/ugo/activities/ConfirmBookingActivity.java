package wehyphens.com.ugo.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import wehyphens.com.ugo.R;

public class ConfirmBookingActivity extends AppCompatActivity implements OnMapReadyCallback {
    ImageView hamburger_menu;
    LinearLayout ll_select_rider;
    RelativeLayout rl_pick_location;
    EditText et_loc_distination;
    Fragment fragmentMap;
    TextView tv_user;
    ImageView iv_who_ride,iv_plus;
    boolean flag=false;
    private GoogleMap mMap;
    private final int PICK_CONTACT=1;

    @RequiresApi(api = Build.VERSION_CODES.O)

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_booking);
        hamburger_menu=findViewById(R.id.hamburger_menu);

        hamburger_menu.setImageResource(R.drawable.arr);


      //  et_loc_distination=findViewById(R.id.et_loc_distination);
      //  rl_pick_location=findViewById(R.id.rl_pick_location);


        ll_select_rider=(LinearLayout)findViewById(R.id.ll_select_rider);
        tv_user=findViewById(R.id.tv_user);

        iv_who_ride=findViewById(R.id.iv_drop_down_user);
        iv_plus=findViewById(R.id.iv_plus);

       iv_plus.setOnClickListener((View.OnClickListener) this);

        iv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getApplicationContext(), "Clicked",
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
              startActivityForResult(intent, PICK_CONTACT);


            }
        });



        iv_who_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(flag==false){

                    ll_select_rider.setVisibility(View.VISIBLE);
                    tv_user.setText("Switch User");
                    flag=true;
                }else {

                    ll_select_rider.setVisibility(View.GONE);
                    tv_user.setText("For Users");
                    flag=false;
                }

            }
        });

        hamburger_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        initializeMap();



    }
    private void initializeMap() {
        if (mMap == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFrag.getMapAsync(this);
        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();
    }

    private void setUpMap() {
        LatLng TutorialsPoint = new LatLng(28.535517, 77.391029);
        mMap.addMarker(new

                MarkerOptions().position(TutorialsPoint).title("Wehyphens.com"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(TutorialsPoint));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(TutorialsPoint)
                .zoom(14).build();
        //Zoom in and animate the camera.
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        // mMap.animateCamera( CameraUpdateFactory.zoomTo( 8.0f ) );

    }
}
