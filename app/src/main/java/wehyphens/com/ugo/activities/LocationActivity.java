package wehyphens.com.ugo.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import wehyphens.com.ugo.R;

public class LocationActivity extends AppCompatActivity implements View.OnClickListener {
Context mContext;
TextView header_two_title;
ImageView iv_back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_location);


        iv_back=findViewById(R.id.iv_back);
      iv_back.setOnClickListener(this);
        mContext=LocationActivity.this;
        header_two_title=findViewById(R.id.header_two_title);
        header_two_title.setText("Locations");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.iv_back:
                onBackPressed();
                break;

        }
    }
}
