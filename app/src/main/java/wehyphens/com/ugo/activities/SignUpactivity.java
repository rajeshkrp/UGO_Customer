package wehyphens.com.ugo.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Callback;
import wehyphens.com.ugo.Adapters.StateAdapterDialog;
import wehyphens.com.ugo.DataInterface.FileUploadInterface;
import wehyphens.com.ugo.Models.CountryCodeDetails;
import wehyphens.com.ugo.Models.CountryCodeResponse;
import wehyphens.com.ugo.Models.CountryData;
import wehyphens.com.ugo.R;
import wehyphens.com.ugo.fragments.FragmentHome;
import wehyphens.com.ugo.interfaces.StateInterface;
import wehyphens.com.ugo.retrofit.RetrofitHandler;
import wehyphens.com.ugo.utils.AppConstants;
import wehyphens.com.ugo.utils.CommonUtils;

public class SignUpactivity extends AppCompatActivity implements StateInterface {
    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    RelativeLayout relative;
    LinearLayout ll_gogle_fb;
    FloatingActionButton civ_next;
    AppBarLayout appbar;
    EditText et_mobile_no;
    ImageView img_spinner,iv_country_flag,iv_ugoo;
    TextView tv_county_code;
    Context context;
    ArrayList<String> countryList;
    ArrayList<String> countryCodeList;
    ProgressDialog progressDialog;
    Spinner spinner;
    AlertDialog.Builder builder;
    AlertDialog  alertDialog;
    EditText et_country_name;
    ImageView iv_back_arrow;
    private static final int TYPE_STATE = 0, TYPE_CITY = 1;
    private Dialog dialog;
    String flag_Pic="";
    private String stateName = "", state_id = "";
    private boolean isClicked=false;
    String phoneNumber="";
    String mnbrsend="";
    TextView ggl_btn;
    String code;
    Button fb_btn2;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    CoordinatorLayout coordinatorLayout;


    //facebook
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    //And also a Firebase Auth object
    FirebaseAuth mAuth;

    //a constant for detecting the login intent result
    private static final int RC_SIGN_IN = 234;

    //Tag for the logs optional
    private static final String TAG = "coding";

    //creating a GoogleSignInClient object
    GoogleSignInClient mGoogleSignInClient;


    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            String name=Profile.getCurrentProfile().getName().toString();


           // displayMessage(profile);
           // Intent intent=new Intent(SignUpactivity.this,ReadyToGoActivity.class);

            Intent intent=new Intent(SignUpactivity.this,ReadyToGoActivity.class);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Window w = getWindow();
                w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            }


        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {

        }

    };


    @SuppressLint({"ClickableViewAccessibility", "WrongViewCast"})
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_signup);
        context=SignUpactivity.this;
        relative = findViewById(R.id.relative);
        et_country_name=findViewById(R.id.et_country_name);
        iv_back_arrow=findViewById(R.id.iv_back_arrow);
        ggl_btn=findViewById(R.id.gglbtn);
      //  fb_btn2=findViewById(R.id.fb_btn2);
        coordinatorLayout=findViewById(R.id.rootLayout);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(true);

         iv_ugoo = (ImageView) findViewById(R.id.iv_ugo);
       Animation aniSlide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_left);
       iv_ugoo.startAnimation(aniSlide);

        //first we intialized the FirebaseAuth object
        mAuth = FirebaseAuth.getInstance();

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

//toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.arr));
        iv_country_flag=findViewById(R.id.iv_country_flag);
        tv_county_code=findViewById(R.id.tv_county_code);
        appbar = findViewById(R.id.appbar);
        et_mobile_no = findViewById(R.id.et_mobile_no);
        img_spinner=findViewById(R.id.img_spinner);
        civ_next =findViewById(R.id.civ_next);
        ll_gogle_fb =findViewById(R.id.ll_gogle_fb);
        countryList=new ArrayList<>();
        countryCodeList=new ArrayList<>();
        spinner=findViewById(R.id.spinnerCountries);
//        fb_btn2=findViewById(R.id.fb_btn2);

//fb_btn2=(LoginButton)findViewById(R.id.fb_btn2);

 LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
        //loginButton.setReadPermissions("user_friends");

        callbackManager = CallbackManager.Factory.create();

      //  loginButton.registerCallback(callbackManager,callback);
        loginButton.registerCallback(callbackManager,callback);



        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                CountryData.countryNames));


        printHashKey();




        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

      profileTracker=new ProfileTracker() {
          @Override
          protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

              displayMessage(currentProfile);

          }
      };

      accessTokenTracker.startTracking();
      profileTracker.startTracking();

//        android.support.v4.app.FragmentManager fm=getSupportFragmentManager();
//        FragmentHome fragmentHome=new FragmentHome();
//        fm.beginTransaction().replace(R.id.rootLayout,fragmentHome).commit();

        //Then we need a GoogleSignInOptions object
        //And we need to build it as below




        GoogleSignInOptions gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        //Then we will get the GoogleSignInClient object from GoogleSignIn class

        mGoogleSignInClient=GoogleSignIn.getClient(this,gso);

        //Now we will attach a click listener to the sign_in_button
        //and inside onClick() method we are calling the signIn() method that will open
        //google sign in intent

        ggl_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();

            }
        });

        img_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                calCountryCode();
            }
        });

        et_mobile_no.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                appbar.setExpanded(false);
                civ_next.setVisibility(View.VISIBLE);
                ll_gogle_fb.setVisibility(View.GONE);
                return false;
            }
        });

        relative.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                appbar.setExpanded(false);
                Log.d("TAG","value of bollean>>"+isClicked);
                System.out.println(isClicked);
                civ_next.setVisibility(View.VISIBLE);
                ll_gogle_fb.setVisibility(View.GONE);



                ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

                return false;
            }
        });


        et_mobile_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });


        iv_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appbar.setExpanded(true);
                civ_next.setVisibility(View.GONE);
                ll_gogle_fb.setVisibility(View.VISIBLE);

                isClicked=true;

                Log.d("A=TAG","value is::"+isClicked);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_mobile_no.getWindowToken(), 0);
            }
        });
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = true;
                    et_mobile_no.setCursorVisible(true);
                    isClicked=true;
                   // actionBar.setDisplayHomeAsUpEnabled(true);
                    iv_back_arrow.setVisibility(View.VISIBLE);
                    iv_ugoo.setVisibility(View.GONE);

                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                    et_mobile_no.setCursorVisible(false);
                    isClicked=false;
                   // actionBar.setDisplayHomeAsUpEnabled(false);
                    iv_back_arrow.setVisibility(View.GONE);
                    iv_ugoo.setVisibility(View.VISIBLE);

                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItemText = (String) adapterView.getItemAtPosition(i);
//                Toast.makeText
//                        (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
//                        .show();
                code = CountryData.countryAreaCodes[spinner.getSelectedItemPosition()];
                tv_county_code.setText("+"+code);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        civ_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                code = CountryData.countryAreaCodes[spinner.getSelectedItemPosition()];
//               tv_county_code.setText("+"+code);


                //hide keyboad
                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);


                String number = et_mobile_no.getText().toString().trim();

                //validation
                if(number.isEmpty() || number.length() < 10){
                    //et_mobile_no.setError("Enter a valid mobile");

                    if(number.isEmpty() || number.length() < 10) {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "Enter a valid mobile number !", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    et_mobile_no.setError(null);
                    et_mobile_no.requestFocus();
                    return;
                }

//                Intent intent1=new Intent(SignUpactivity.this,OtpActivity.class);
//                mnbrsend=et_mobile_no.getText().toString();
//                intent1.putExtra("Value",mnbrsend);
//                startActivity(intent1);

                phoneNumber = "+" + code + number;
                CommonUtils.savePreferencesString(context,AppConstants.USER_PHONE2,phoneNumber);
                //cahnge activity
                Intent intent = new Intent(SignUpactivity.this,OtpActivity.class);
                intent.putExtra("phonenumber", phoneNumber);
                startActivity(intent);

                //animation
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

       /* CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                return false;
            }
        });
*/


      //  dynamicToolbarColor();
        toolbarTextAppernce();

    }



    private void displayMessage(Profile currentProfile) {


       if(currentProfile != null){
         //  tv.setText(profile.getName());

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = mAuth.getCurrentUser();

        if (user != null) {
            finish();
            startActivity(new Intent(SignUpactivity.this, DashBoardActivity.class));
        }


    }

    public void printHashKey(){
        // Add code to print out the facebook key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "wehyphens.com.ugo",

    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(signature.toByteArray());
        Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
    }
} catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        }
    //this method is called on click
    private void signIn() {

        //getting the google signin intent
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();

        //starting the activity for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            Intent intent = new Intent(this, DashBoardActivity.class);
           intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);startActivity(intent);

           if (mAuth.getCurrentUser() !=null)
           {
               finish();
               startActivity(new Intent(this,DashBoardActivity.class));
           }else if(mAuth.getCurrentUser()==null)

           {
               startActivity(new Intent(this,SignUpactivity.class));
           }
        }

        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
        {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if the requestCode is the Google Sign In code that we defined at starting

        if(requestCode==RC_SIGN_IN)
        {
            //Getting the GoogleSignIn Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            try {
                //Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);

                //authenticating with firebase
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Toast.makeText(SignUpactivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        //getting the auth credential
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        //Now using firebase we are signing in the user here
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            String userName=user.getDisplayName();
                            String userEmail=user.getEmail();
                            String userPhone=user.getPhoneNumber();
                            Uri userPhoto=user.getPhotoUrl();
                         //   CommonUtils.savePreferencesString(context,AppConstants.USER_PHOTO.toString(),userPhoto.toString());
                            CommonUtils.savePreferencesString(context,AppConstants.USER_NAME,userName);
                            CommonUtils.savePreferencesString(context,AppConstants.USER_EMAIL,userEmail);
                            CommonUtils.savePreferencesString(context,AppConstants.USER_PHONE3,userPhone);
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                            Boolean GoogleLocked = prefs.edit().putBoolean("GoogleLocked", true).commit();
                            Toast.makeText(SignUpactivity.this, "Signed In Successful", Toast.LENGTH_SHORT).show();
                           Intent intentact=new Intent(SignUpactivity.this,ReadyToGoActivity.class);
                           startActivity(intentact);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(SignUpactivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });

    }



    private void toolbarTextAppernce() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
    }

        private void dynamicToolbarColor() {

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.homec);
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onGenerated(Palette palette) {
               collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(R.attr.colorPrimary));
               collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(R.attr.colorPrimaryDark));

            }
        });
    }


    private void callLocationSateDialog(List<CountryCodeDetails> list) {
        dialog = new Dialog(context, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_country);
        // dialog.getWindow().getAttributes().windowAnimations = animationdialog;
        ImageView leftarrow = (ImageView) dialog.findViewById(R.id.leftarrow);
        EditText editMobileNo = (EditText) dialog.findViewById(R.id.etSearch);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        final ListView lv = (ListView) dialog.findViewById(R.id.listview);
        TextView saveImage = (TextView) dialog.findViewById(R.id.saveImage);
        StateAdapterDialog stateAdapterDialog = new StateAdapterDialog(SignUpactivity.this, list, this, TYPE_STATE);
        lv.setAdapter(stateAdapterDialog);
        if (state_id != null) {
            stateAdapterDialog.setSelection(state_id);
            dialog.dismiss();

        }
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        editMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                stateAdapterDialog.getFilter().filter(arg0);
                stateAdapterDialog.getViewTypeCount();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
        dialog.show();
    }


    private void calCountryCode() {


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("loading");
        progressDialog.show();

        // CommonUtils.showProgress(context);
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<CountryCodeResponse> call = service.getCountry_Code();
        call.enqueue(new Callback<CountryCodeResponse>() {
            @Override
            public void onResponse(retrofit2.Call<CountryCodeResponse> call, retrofit2.Response<CountryCodeResponse> response) {

                CountryCodeResponse CountryCodeResponse = response.body();
                countryList.clear();
                countryCodeList.clear();
                if (CountryCodeResponse != null && CountryCodeResponse.getData().size() > 0) {
                    // List<String> countryCodeList = new ArrayList<>();

                    callLocationSateDialog(CountryCodeResponse.getData());

                } else {
                    CommonUtils.snackBar(" Not Fetched", et_mobile_no);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<CountryCodeResponse> call, Throwable t) {
                CommonUtils.snackBar("check your internet connection",et_mobile_no);
            }
        });


    }



    @Override
    public void setValue(String name, String country_code, String flag_pic,boolean selected) {
        state_id=country_code;
        stateName=name;
        flag_Pic=flag_pic;
             flag_Pic=flag_pic.toLowerCase();




        Log.e("profile_pic","profile_pic"+flag_Pic);
        String url="https://api.backendless.com/2F26DFBF-433C-51CC-FF56-830CEA93BF00/473FB5A9-D20E-8D3E-FF01-E93D9D780A00/files/CountryFlagsPng/"+flag_Pic+".png";
        Log.e("URL",url);
        Picasso.with(context).load(url).into(iv_country_flag);
        //iv_country_flag.setImageURI();


       //Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(iv_country_flag);

        tv_county_code.setText("+"+countryCodeList);
//        dialog.dismiss();
//        progressDialog.dismiss();


    }

    @Override
    public void setValue(List<CountryCodeDetails> currentSelected, int type) {

    }
}
