package wehyphens.com.ugo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import wehyphens.com.ugo.R;

public class ThemeAcitivity extends AppCompatActivity implements View.OnClickListener{
    Button btn_light,btn_dark;
    ImageView iv_next;
    RelativeLayout rl_main;
    TextView  tv_chose,tv_pic_theme;
Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);
        context=ThemeAcitivity.this;

        btn_dark=findViewById(R.id.btn_dark);
        btn_light=findViewById(R.id.btn_light);
        iv_next=findViewById(R.id.iv_next);
        tv_pic_theme=findViewById(R.id.tv_theme);
        tv_chose=findViewById(R.id.tv_chose);
        rl_main=findViewById(R.id.rl_main);



        iv_next.setOnClickListener(this);
        btn_light.setOnClickListener(this);
        btn_dark.setOnClickListener(this);



    }

    public void theme(View view){

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn_dark:
                tv_pic_theme.setTextColor(getResources().getColor(R.color.white));
                tv_chose.setTextColor(getResources().getColor(R.color.white));
                rl_main.setBackgroundColor(getResources().getColor(R.color.colorBlack));

               // iv_next.setColorFilter(Color.WHITE, PorterDuff.Mode.LIGHTEN);
               // iv_next.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                iv_next.setImageResource(R.drawable.ic_forword_right_white_24dp);
                break;

            case R.id.btn_light:
                tv_pic_theme.setTextColor(getResources().getColor(R.color.colorBlack));
                tv_chose.setTextColor(getResources().getColor(R.color.colorBlack));
                rl_main.setBackgroundColor(getResources().getColor(R.color.white));

        iv_next.setImageResource(R.drawable.arrr);
                break;

            case R.id.iv_next:

                startActivity(new Intent(context, SignUpactivity.class));
                break;
        }



    }
}
