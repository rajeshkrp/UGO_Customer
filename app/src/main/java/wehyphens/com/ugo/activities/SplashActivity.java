package wehyphens.com.ugo.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import wehyphens.com.ugo.R;

public class SplashActivity extends AppCompatActivity{
Context context;
ImageView img_anim;
static long SPLASH_TIME_OUT=4000;
View view;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = SplashActivity.this;


        ImageView img = (ImageView) findViewById(R.id.splash);
//        Animation aniSlide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
//        img.startAnimation(aniSlide);

        Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
        Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
        img.setAnimation(zoomin);
        img.setAnimation(zoomout);


       // img_anim = findViewById(R.id.splash_img);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(context, SignUpactivity.class);
                startActivity(intent);
                finish();

            }
        }, SPLASH_TIME_OUT);


    }


}
