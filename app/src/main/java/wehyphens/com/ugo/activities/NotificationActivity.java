package wehyphens.com.ugo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import wehyphens.com.ugo.R;

public class NotificationActivity extends AppCompatActivity {
Context mContext;
TextView header_two_title;
RelativeLayout rl_ac__updates,rl_dis_news;
ImageView iv_back;

    public void  onCreate(Bundle savedInseTanceState) {
        setContentView(R.layout.fragment_mobile_notificatyions);
        super.onCreate(savedInseTanceState);
        mContext=NotificationActivity.this;
        header_two_title=findViewById(R.id.header_two_title);
        header_two_title.setText("Mobile Notifications");
        iv_back=findViewById(R.id.iv_back);

        rl_dis_news=findViewById(R.id.rl_dis_news);
        rl_ac__updates=findViewById(R.id.rl_ac__updates);


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        rl_ac__updates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext,AccountTripUpdateUpdate.class));

                           }
        });

        rl_dis_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext,DiscountAndNewsActivity.class));

            }
        });

        header_two_title.setText("Mobile Notification");

    }

}
