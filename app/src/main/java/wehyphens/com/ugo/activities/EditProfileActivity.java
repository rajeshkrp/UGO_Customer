package wehyphens.com.ugo.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.ugo.R;
import wehyphens.com.ugo.utils.AppConstants;
import wehyphens.com.ugo.utils.CommonUtils;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private static final String TAG = "coding";
    TextView tv_edit, header_two_title;
    EditText et_f_name, et_l_name, et_phon_no, et_password, et_email;
    ImageView fabtn_edt_pic;
    RelativeLayout rl_edit_pic;
    ImageView iv_back,mnotverify,mnotverify2;
    LinearLayout ll_chng_pass;
    boolean isClicked = false;
    Context mContext;
    CircleImageView cv_profile_pic;
    FirebaseAuth mAuth;
    FirebaseUser mUser;
    TextView tv_username;
    TextView tv_useremail;
    Button mverfyEmail;
    LinearLayout linearLayout;
    Boolean emailCheck;
    EditText mpass;
    GoogleApiClient mGoogleApiClient;
    Button bt_phn_cahnge;
    String myEmail = "";
    String phoneNumber = "";
    boolean check;
    ImageView verifypic;
    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA};
    private int GALLERY = 1, CAMERA = 2;

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        tv_edit = findViewById(R.id.tv_edit);
        et_f_name = findViewById(R.id.et_f_name);
        et_l_name = findViewById(R.id.et_l_name);
        et_phon_no = findViewById(R.id.et_phon_no);
        et_email = findViewById(R.id.et_email);
        iv_back = findViewById(R.id.iv_back);
        ll_chng_pass = findViewById(R.id.ll_chng_pass);
        fabtn_edt_pic = findViewById(R.id.fabtn_edt_pic);
        header_two_title = findViewById(R.id.header_two_title);
        cv_profile_pic = findViewById(R.id.cv_profile_pic);
        rl_edit_pic = findViewById(R.id.rl_edit_pic);
        mpass = findViewById(R.id.et_pass);
        linearLayout = findViewById(R.id.edit_layout);
        bt_phn_cahnge = findViewById(R.id.bt_phone_chnge);
        verifypic = findViewById(R.id.verifyphonepic);
        mnotverify=findViewById(R.id.notverifypic);
        mnotverify2=findViewById(R.id.notverifypic2);

        header_two_title.setText("Edit Profile");

        tv_edit.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        fabtn_edt_pic.setOnClickListener(this);
        ll_chng_pass.setOnClickListener(this);
        mContext = EditProfileActivity.this;
        bt_phn_cahnge.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        SharedPreferences.Editor editor;
        mnotverify2.setVisibility(View.VISIBLE);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        Boolean yourLocked = prefs.getBoolean("locked", false);
        if (yourLocked) {
            et_phon_no.setText(CommonUtils.getPreferencesString(mContext, AppConstants.USER_PHONE2));
            bt_phn_cahnge.setEnabled(false);
            et_phon_no.setEnabled(false);
            verifypic.setVisibility(View.VISIBLE);
            mnotverify.setVisibility(View.GONE);
        } else {
            et_phon_no.setText(CommonUtils.getPreferencesString(mContext, AppConstants.USER_PHONE2));
            mnotverify.setVisibility(View.VISIBLE);
        }

        Boolean yourGoogleLocked = prefs.getBoolean("GoogleLocked", false);
        if (yourGoogleLocked) {
            bt_phn_cahnge.setVisibility(View.VISIBLE);
            et_phon_no.setText(CommonUtils.getPreferencesString(mContext, AppConstants.USER_PHONE3));
        }

        Boolean yourOtpLocked = prefs.getBoolean("OtpLocked", false);

        if (yourOtpLocked) {
            et_phon_no.setText(CommonUtils.getPreferencesString(mContext, AppConstants.USER_PHONE));
            et_phon_no.setEnabled(false);
            verifypic.setVisibility(View.VISIBLE);
            bt_phn_cahnge.setVisibility(View.GONE);
            mnotverify.setVisibility(View.GONE);
            et_phon_no.setError(null);
        }

        bt_phn_cahnge.setOnClickListener(new View.OnClickListener() {
            @Override

            //Disabled
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);


                String number = et_phon_no.getText().toString().trim();

                //validation
                if (number.isEmpty() || number.length() < 10) {
                    //et_mobile_no.setError("Enter a valid mobile");

                    if (number.isEmpty() || number.length() < 10) {
                        Snackbar snackbar = Snackbar
                                .make(linearLayout, "Enter a valid mobile number !", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    et_phon_no.setError(null);
                    et_phon_no.requestFocus();
                    return;
                }
                phoneNumber = "+91" + number;
                CommonUtils.savePreferencesString(mContext, AppConstants.USER_PHONE, phoneNumber);
                //cahnge activity
                Intent intent = new Intent(EditProfileActivity.this, EditOtpActivity.class);
                intent.putExtra("phonenumber", phoneNumber);
                startActivity(intent);

                //animation
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

    }

    private void sendVerificationcode(String myEmail) {
        //    myEmail = et_email.getText().toString().trim();
        mUser.sendEmailVerification().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(), "Code Send to " + mUser.getEmail(), Toast.LENGTH_SHORT).show();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Toast.makeText(getApplicationContext(), "Code Failed" + mUser.getEmail(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();


//        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
//            Intent intent = new Intent(this, EditOtpActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
//            startActivity(intent);
//        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_f_name:
                break;
            case R.id.tv_edit:
                if (isClicked == false) {
                    et_f_name.setEnabled(true);
                    et_l_name.setEnabled(true);
                    et_email.setEnabled(true);
                    et_phon_no.setEnabled(true);
                    rl_edit_pic.setVisibility(View.VISIBLE);
                    isClicked = true;
                    tv_edit.setText("Save");
                } else {
                    et_f_name.setEnabled(false);
                    et_l_name.setEnabled(false);
                    et_email.setEnabled(false);
                    et_phon_no.setEnabled(false);
                    rl_edit_pic.setVisibility(View.GONE);
                    tv_edit.setText("Edit");
                    isClicked = false;
                }

                break;

            case R.id.fabtn_edt_pic:

                showPictureDialog();


                break;

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.ll_chng_pass:

                startActivity(new Intent(mContext, ChangePasswordActivity.class));

                break;
            case R.id.et_l_name:

                break;

            case R.id.et_phon_no:

                break;

            case R.id.et_email:

                break;

            case R.id.bt_phone_chnge:

//                String number = et_phon_no.getText().toString().trim();
//
//                if (number.isEmpty() || number.length() < 10) {
//                    et_phon_no.setError("Valid number is required");
//                    et_phon_no.requestFocus();
//                    return;
//                }
//
//                String phoneNumber = "+91"+number;
//                Intent intent = new Intent(EditProfileActivity.this, EditOtpActivity.class);
//                intent.putExtra("phonenumber", phoneNumber);
//                startActivity(intent);
                break;
        }

    }

    private void VerifyEmail() {
        FirebaseUser user = mAuth.getCurrentUser();
        emailCheck = user.isEmailVerified();
        if (emailCheck) {

        } else {
            Toast.makeText(getApplicationContext(), "Not verifyed", Toast.LENGTH_SHORT).show();
            mAuth.signOut();
        }

    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera",
                "Cancel"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 2:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {


       /* if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){

        }

            ActivityCompat.requestPermissions(EditProfileActivity.this, new String[] {android.Manifest.permission.CAMERA}, CAMERA);
*/

        if (!hasPermissions(mContext, PERMISSIONS)) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, CAMERA);
            }

            // this.requestPermissions(getActivity(), PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {

            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(EditProfileActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    cv_profile_pic.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(EditProfileActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            cv_profile_pic.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(EditProfileActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


}



