package wehyphens.com.ugo.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import wehyphens.com.ugo.R;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener{
    ImageView iv_back;
    TextView header_two_title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        iv_back=findViewById(R.id.iv_back);
        header_two_title=findViewById(R.id.header_two_title);
        header_two_title.setText("Change Password");

        iv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.iv_back:

onBackPressed();
                break;


        }
    }
}
