package wehyphens.com.ugo.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import wehyphens.com.ugo.R;
import wehyphens.com.ugo.fragments.FragmentHome;


public class OtpActivity extends AppCompatActivity {


    ImageView iv_back;
    TextView header_title, tv_timer, tv_second;
    FloatingActionButton fabtn_next;
    Context mContext;
    LinearLayout ll_header_view;
    Context context;
    private Handler handler = new Handler();
    private Runnable runnable;
    private String EVENT_DATE_TIME = "2018-12-31 10:30:00";
    private String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    String otp;
    PinEntryEditText entryEditText;
    String phoneNumber="";
    FirebaseAuth auth;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private String verificationCode;
    private String mVerificationId;
    private FirebaseAuth mAuth;
    TextView mtvSend;
    String mnbrsend="";
    ProgressBar progressBar ;
    private String verificationId;
    LinearLayout linearLayout;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        mContext = OtpActivity.this;
        fabtn_next = findViewById(R.id.fabtn_next);
        iv_back = findViewById(R.id.iv_back);
        header_title = findViewById(R.id.header_two_title);
        ll_header_view = findViewById(R.id.ll_header_view);
        tv_timer = findViewById(R.id.tv_timer);
        tv_second = findViewById(R.id.tv_second);
        context = OtpActivity.this;
        entryEditText = findViewById(R.id.txt_pin_entry);
        mAuth = FirebaseAuth.getInstance();
        mtvSend=findViewById(R.id.tv_send);
        progressBar=findViewById(R.id.otp_prog);
        linearLayout =findViewById(R.id.liner_otp);

        fabtn_next.setEnabled(false);

//       mnbrsend=getIntent().getExtras().getString("Value");
//       mtvSend.setText(mnbrsend);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        ll_header_view.setBackgroundColor(Color.WHITE);
        iv_back.setColorFilter(ContextCompat.getColor(context, R.color.colorBlack), android.graphics.PorterDuff.Mode.MULTIPLY);
        header_title.setTextColor(Color.BLACK);
        header_title.setText("OTP");
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        phoneNumber = getIntent().getStringExtra("phonenumber");
        sendVerificationCode(phoneNumber);

        new Handler().postDelayed(new Runnable()
                                  {
                                      public void run()
                                      {
                                          fabtn_next.setEnabled(true);
                                          fabtn_next.setBackgroundTintList(getResources().getColorStateList(R.color.backtintcolr));
                                      }
                                  }, 4000    //Specific time in milliseconds
        );



        tv_timer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendVerificationCode(phoneNumber);
                Toast.makeText(getApplicationContext(),"Code Resend successfully.",Toast.LENGTH_SHORT).show();

            }
        });

        fabtn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                String code = entryEditText.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    //entryEditText.setError("Enter valid code");

                    if(code.isEmpty() || code.length() < 6) {
                        Snackbar snackbar = Snackbar
                                .make(linearLayout, "Enter a valid Code !", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                    entryEditText.setError(null);
                    entryEditText.requestFocus();
                    return;
                }

                //verifying the code entered manually
                verifyCode(code);

                //       startActivity(new Intent(mContext, ReadyToGoActivity.class));
            }
        });

        entryEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.GONE);
            }
        });


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        new CountDownTimer(121000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                tv_timer.setText(" " + String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                tv_timer.setText(" Click Here");
            }

        }.start();
    }



    private void verifyCode(String code) {
        PhoneAuthCredential credential = null;
        try {
            credential = PhoneAuthProvider.getCredential(verificationId, code);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            signInWithCredential(credential);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks
        );
    }




    //the callback to detect the verification status

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                entryEditText.setText(code);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

            Snackbar snackbar = Snackbar
                    .make(linearLayout, "Enter a valid Code !", Snackbar.LENGTH_LONG);
            snackbar.show();
         //  Toast.makeText(getApplicationContext(),"Please Enter Correct Pin !",Toast.LENGTH_SHORT).show();

        }
    };


    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                            Boolean statusLocked = prefs.edit().putBoolean("locked", true).commit();
                            Snackbar snackbar = Snackbar
                                    .make(linearLayout, "OTP Confirmed", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            Intent intent = new Intent(OtpActivity.this, ReadyToGoActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            startActivity(intent);

                        } else {

                            Snackbar snackbar = Snackbar
                                    .make(linearLayout, "Enter a valid Code !", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            //Toast.makeText(getApplicationContext(),"Please Enter Correct Pin !",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

//    @Override
//    public void finish() {
//        super.finish();
////        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//    }


}
