package wehyphens.com.ugo.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import wehyphens.com.ugo.R;
import wehyphens.com.ugo.utils.AppConstants;
import wehyphens.com.ugo.utils.CommonUtils;

public class EditOtpActivity extends AppCompatActivity {


    private String verificationId;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;
    private EditText editTextode;
    Context context;
     RelativeLayout relativeLayout;
    String phoneNumber="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_otp);

        context=EditOtpActivity.this;
        mAuth = FirebaseAuth.getInstance();

        progressBar = findViewById(R.id.progressbar);
        editTextode = findViewById(R.id.editTextCode);
        relativeLayout=findViewById(R.id.edit_layoutotp);

        phoneNumber = getIntent().getStringExtra("phonenumber");
        sendVerificationCode(phoneNumber);

        findViewById(R.id.buttonSignIn).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                String code = editTextode.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    //entryEditText.setError("Enter valid code");

                    if(code.isEmpty() || code.length() < 6) {
                        Snackbar snackbar = Snackbar
                                .make(relativeLayout, "Enter a valid Code !", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                    editTextode.setError(null);
                    editTextode.requestFocus();
                    return;
                }

                //verifying the code entered manually
                verifyCode(code);
            }
        });

    }
    private void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                            Boolean OtpLocked = prefs.edit().putBoolean("OtpLocked", true).commit();
                        //    CommonUtils.savePreferencesString(context,AppConstants.USER_PHONE,phoneNumber);
                            //saving value
//                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
//                            Boolean statusLocked = prefs.edit().putBoolean("locked", true).commit();


                            Intent intent = new Intent(EditOtpActivity.this, EditProfileActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            startActivity(intent);

                        } else {
                           // Toast.makeText(EditOtpActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
//                            Snackbar snackbar = Snackbar
//                                    .make(relativeLayout, "Enter a valid Code !", Snackbar.LENGTH_LONG);
//                            snackbar.show();
                        }
                    }
                });
    }

    private void sendVerificationCode(String number) {
        progressBar.setVisibility(View.VISIBLE);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        );

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                editTextode.setText(code);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
        //    Toast.makeText(EditOtpActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            Snackbar snackbar = Snackbar
                    .make(relativeLayout, "Enter a valid Code !", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    };
}
