package wehyphens.com.ugo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import wehyphens.com.ugo.R;

public class ReadyToGoActivity extends AppCompatActivity {
    FloatingActionButton fabtn_next;
    Context mContext;
    ImageView iv_car;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ready_to_move);

        fabtn_next=findViewById(R.id.fabtn_next);
        mContext=ReadyToGoActivity.this;

       iv_car=findViewById(R.id.iv_car);
        final Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        iv_car.startAnimation(anim);



                fabtn_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    startActivity(new Intent(mContext,DashBoardActivity.class));

                     //   overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

//                        iv_car=findViewById(R.id.iv_car);
//                        Animation aniSlide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
//                        iv_car.startAnimation(aniSlide);
                        startActivity(new Intent(mContext,DashBoardActivity.class));

                        fabtn_next.startAnimation(anim);
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {

                                startActivity(new Intent(mContext,DashBoardActivity.class));
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {


                            }
                        });
                    }
                });


        InputMethodManager imm = (InputMethodManager) getSystemService(mContext.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager immm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


//    @Override
//    public void onClick(View view) {
//        switch (view.getId()){
//            case R.id.fabtn_next:
//                startActivity(new Intent(mContext,DashBoardActivity.class));
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//
//                iv_car=findViewById(R.id.iv_car);
//                Animation aniSlide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
//                iv_car.startAnimation(aniSlide);
//
//                break;
//        }
//    }



}
