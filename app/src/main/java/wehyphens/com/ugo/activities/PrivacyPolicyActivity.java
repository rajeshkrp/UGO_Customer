package wehyphens.com.ugo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import wehyphens.com.ugo.R;
import wehyphens.com.ugo.fragments.FragDeleteAccount;

public class PrivacyPolicyActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView iv_back;
    RelativeLayout rl_del_account,rl_location,rl_notification;
    LinearLayout ll_container;
    FragmentManager manager;
    FragmentTransaction transaction;
    TextView header_two_title;
    Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        iv_back=findViewById(R.id.iv_back);
        rl_del_account=findViewById(R.id.rl_del_account);
        rl_notification=findViewById(R.id.rl_notification);
        rl_location=findViewById(R.id.rl_location);
        header_two_title=findViewById(R.id.header_two_title);
        mContext=PrivacyPolicyActivity.this;

        header_two_title.setText("Privacy Settings");
        iv_back.setOnClickListener(this);
        rl_del_account.setOnClickListener(this);
        rl_location.setOnClickListener(this);
        rl_notification.setOnClickListener(this);




    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.rl_del_account:
                startActivity(new Intent(mContext,DeleteAccountActivity.class));

                break;
            case R.id.rl_notification:
             startActivity(new Intent(mContext,NotificationActivity.class));


                break;
            case R.id.rl_location:
              startActivity(new Intent(mContext,LocationActivity.class));
                break;

        }
    }
   /* @Override
    public void onBackPressed(){
        if (getSupportFragmentManager().getBackStackEntryCount()>0){
            getSupportFragmentManager().popBackStack();
        }
        else {
            super.onBackPressed();
        }
    }*/
}
