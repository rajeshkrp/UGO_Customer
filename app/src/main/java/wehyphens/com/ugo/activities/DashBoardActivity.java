package wehyphens.com.ugo.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.ugo.R;
import wehyphens.com.ugo.fragments.FragSetting;
import wehyphens.com.ugo.fragments.FragmentHome;
import wehyphens.com.ugo.fragments.FragmentPayment;
import wehyphens.com.ugo.fragments.FragmentYourTrips;
import wehyphens.com.ugo.fragments.TabFragmenPast;
import wehyphens.com.ugo.utils.AppConstants;
import wehyphens.com.ugo.utils.CommonUtils;

public class DashBoardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    ImageView hamburger_menu, iv_user_logo, iv_who_ride, header_car;
    TextView tv_user, header_title;
    LinearLayout ll_header_view;
    DrawerLayout drawer;
    NavigationView navigationView;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    CircleImageView profile;
    Context mContext;
    TextView textName, textEmail;
    FirebaseAuth mAuth;
    GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        mContext = DashBoardActivity.this;
        fragmentManager = getSupportFragmentManager();
        hamburger_menu = findViewById(R.id.hamburger_menu);
        iv_user_logo = findViewById(R.id.iv_user_logo);
        iv_who_ride = findViewById(R.id.iv_drop_down_user);
        tv_user = (TextView)findViewById(R.id.tv_user);
        header_car = findViewById(R.id.header_car);
        header_title = findViewById(R.id.header_title);
        ll_header_view = findViewById(R.id.ll_header_view);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
       TextView tv_useremail=navigationView.getHeaderView(0).findViewById(R.id.tv_userEmailad);
        profile = navigationView.getHeaderView(0).findViewById(R.id.cv_profile_pic);
        FragmentHome fragmentHome = new FragmentHome();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragmentHome);
        fragmentTransaction.commit();


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, EditProfileActivity.class));
                drawer.closeDrawer(GravityCompat.START);

            }
        });

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user=mAuth.getCurrentUser();

//      Glide.with(this)
//                .load(user.getPhotoUrl())
//              .into(iv_user_logo);
//
//        Glide.with(this)
//                .load(user.getPhotoUrl())
//                .into(profile);



        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        Boolean yourLocked = prefs.getBoolean("OtpLocked", false);

        if (yourLocked)
        {
            tv_useremail.setText(CommonUtils.getPreferencesString(mContext, AppConstants.USER_PHONE));
        }
        else
        {
            tv_useremail.setText("Verify Your Phone !");
        }

        tv_user .setText(user.getDisplayName());
       // tv_useremail.setText(user.getDisplayName());
     //   tv_useremail.setText(CommonUtils.getPreferencesString(mContext,AppConstants.USER_PHONE2));


        hamburger_menu.setOnClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    protected void onStart() {

        super.onStart();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
        if (mAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, SignUpactivity.class));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            ll_header_view.setVisibility(View.VISIBLE);
            header_title.setVisibility(View.GONE);


            FragmentHome fragmentHome = new FragmentHome();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragmentHome);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_your_trips) {
            ll_header_view.setVisibility(View.GONE);
            header_title.setVisibility(View.VISIBLE);
            header_title.setText("Your Trips");


            FragmentYourTrips fragment = new FragmentYourTrips();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
            // Handle the camera action
        } else if (id == R.id.nav_payments) {
            ll_header_view.setVisibility(View.GONE);
            header_title.setVisibility(View.VISIBLE);
            header_title.setText("Payment");



            FragmentPayment payment=new FragmentPayment();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, payment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_help) {

        } else if (id == R.id.nav_setting) {


            ll_header_view.setVisibility(View.GONE);
            header_title.setVisibility(View.VISIBLE);
            header_title.setText("Settings");

            FragSetting fragSetting = new FragSetting();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragSetting);
            fragmentTransaction.commit();



        } else if (id == R.id.nav_logout)
                 {
                     SharedPreferences prefss = PreferenceManager.getDefaultSharedPreferences(mContext);
                     prefss.edit().clear().commit();
                     signOut();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void signOut()
    {
        mAuth.signOut();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {

                        mGoogleApiClient.disconnect();
                       Log.d("status",status+"");
                      //  Toast.makeText(getApplicationContext(),status+"",Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(getApplicationContext(),SignUpactivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.hamburger_menu:
                drawer.openDrawer(Gravity.START);

                break;
        }

    }
    @Override
    public void finish() {
        super.finish();

    }
}
